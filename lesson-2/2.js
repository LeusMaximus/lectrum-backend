class TimersManager {
  constructor() {
    this.timers = [];
    this.isStarted = false;
    this.logs = [];
  }

  add(timerOptions, ...jobArgs) {
    if (this.isStarted) {
      throw new Error('You can not add new timer after start. You can stop the timers and add a new one.');
    }

    this.validateName(timerOptions.name);

    const preparedOptions = {
      ...timerOptions,
      job: this._log(timerOptions.job, timerOptions.name),
    };

    this.timers.push(new Timer(preparedOptions, jobArgs));

    return this;
  }

  remove(timerName) {
    this.pause(timerName);
    this.timers = this.timers.filter(timer => timer.name !== timerName);

    if (!this.timers.length) {
      this.isStarted = false;
    }
  }

  start() {
    this.isStarted = true;
    this.timers.forEach(timer => timer.start());
  }

  stop() {
    this.timers.forEach(timer => timer.stop());
    this.isStarted = false;
  }

  pause(timerName) {
    const timer = this.findByName(timerName);

    if (timer) {
      timer.stop();
    }
  }

  resume(timerName) {
    const timer = this.findByName(timerName);

    if (timer) {
      timer.start();
    }
  }

  findByName(name) {
    return this.timers.find(timer => timer.name === name);
  }

  validateName(timerName) {
    const existingTimer = this.timers.find(timer => timer.name === timerName);

    if (existingTimer) {
      throw new Error(`Timer with ${timerName} name is already exist.`);
    }
  }

  _log(func, timerName) {
    const logs = this.logs;

    return (...args) => {
      const createdAt = new Date();
      const result = func(...args);

      logs.push({
        name: timerName,
        in: args,
        out: result,
        created: createdAt,
      });

      return result;
    };
  }

  print() {
    console.log(this.logs);
  }
}

class Timer {
  constructor(options, jobArgs) {
    this.validate(options);

    this.name = options.name;
    this.delay = options.delay;
    this.interval = options.interval;
    this.job = options.job;
    this.jobArgs = jobArgs;
    this.timer = null;
  }

  start() {
    const timerType = this.interval ? 'setInterval' : 'setTimeout';

    this.timer = global[timerType](this.job, this.delay, ...this.jobArgs);
  }

  stop() {
    if (this.timer) {
      this.timer.close();
    }
  }

  validate(options) {
    let errorMessage = '';

    if (!options.name || typeof options.name !== 'string') {
      errorMessage = `Incorrect timer name ${options.name}. It should be a string`;
    }

    if (typeof options.delay !== 'number') {
      errorMessage = `Incorrect delay format for ${options.name} timer. It should be a number.`;
    }

    if (options.delay < 0 || options.delay > 5000) {
      errorMessage = `${options.delay} - it's wrong delay value for ${options.name} timer. It should be more then 0 and less then 5000.`;
    }

    if (typeof options.interval !== 'boolean') {
      errorMessage = `Incorrect interval format for ${options.name} timer. It should be a boolean.`;
    }

    if (typeof options.job !== 'function') {
      errorMessage = `Incorrect job format for ${options.name} timer. It should be a function.`;
    }

    if (errorMessage) {
      throw new Error(errorMessage);
    }
  }
}

module.exports = TimersManager;
