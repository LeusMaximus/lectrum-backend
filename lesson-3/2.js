const EventEmitter = require('events');

class Bank extends EventEmitter {
  constructor() {
    super();
    this._clientList = [];
    this._handleEvents();
  }

  register(clientData) {
    this._validateClient(clientData);

    const clientId = this._generateId(clientData.name);

    this._clientList.push({
      ...clientData,
      id: clientId
    });

    return clientId;
  }

  _generateId(name) {
    return Symbol(name);
  }

  _getById(id) {
    const client = this._clientList.find(client => client.id === id);

    if (!client) {
      this.emit('error', `Client with '${id}' id doesn't exist.`);
    }

    return client;
  }

  _getByName(name = '') {
    return this._clientList.find(client => client.name.toLowerCase() === name.toLowerCase());
  }

  _onAdd(id, amount) {
    if (!this._isAmountValid(amount)) {
      this.emit('error', `Wrong amount '${amount}'. You can only add a positive amount greater than zero.`);
    }

    this._changeBalance(id, amount);
  }

  _onWithdraw(id, amount) {
    if (!this._isAmountValid(amount)) {
      this.emit('error', `Wrong amount '${amount}'. You must provide a positive amount..`);
    }

    this._changeBalance(id, -amount);
  }

  _onGet(id, cb) {
    const client = this._getById(id);
    cb(client.balance);
  }

  _onSend(clientFirstId, clientSecondId, amount) {
    this._onWithdraw(clientFirstId, amount);
    this._onAdd(clientSecondId, amount);
  }

  _changeBalance(id, amount) {
    const client = this._getById(id);
    const newBalance = client.balance += amount;

    if (newBalance < 0) {
      this.emit('error', `Unable to withdraw ${Math.abs(amount)}. Insufficient funds.`);
    }

    client.balance = newBalance;
  }

  _handleEvents() {
    this.on('add', this._onAdd);
    this.on('get', this._onGet);
    this.on('withdraw', this._onWithdraw);
    this.on('send', this._onSend);
  }

  _validateClient(clientData) {
    const { name, balance } = clientData;
    const existingUser = this._getByName(name);

    if (existingUser) {
      this.emit('error', `Client with ${name} name is already exist.`);
    }

    if (!this._isAmountValid(balance)) {
      this.emit('error', `Wrong balance '${balance}'. The balance must be a number greater than zero.`);
    }
  }

  _isAmountValid(amount) {
    return amount && typeof amount === 'number' && amount > 0;
  }
}

module.exports = Bank;
