const { Readable, Writable, Transform } = require('stream');
const EventEmitter = require('events');

class Ui extends Readable {
  constructor(data, options = { objectMode: true }) {
    super(options);

    this._data = [...data];
  }

  _read() {
    const data = this._data.shift();

    if (data) {
      this._validate(data);
      this.push(data);
    } else {
      this.push(null);
    }
  }

  _validate(data) {
    const requiredFields = ['name', 'email', 'password'];

    requiredFields.forEach(field => {
      const value = data[field];
      const requiredMessage = `User ${field} field is required.`;

      if (!value) {
        this.emit('error', requiredMessage);
      }

      if (typeof value !== 'string') {
        this.emit('error', `The ${field} field is not in the correct format. It should be a string.`);
      }

      if (!value.trim()) {
        this.emit('error', requiredMessage);
      }
    });

    if (requiredFields.length !== Object.keys(data).length) {
      this.emit('error', `Data for ${data.name} user has extra fields`);
    }
  }
}

class Guardian extends Transform {
  constructor(options = { objectMode: true }) {
    super(options);
  }

  _transform(chunk, encoding, done) {
    this.push(new EncryptData(chunk));
    done();
  }
}

class AccountManager extends Writable {
  constructor(options = { objectMode: true }) {
    super(options);
    this.data = [];
  }

  _write(chunk, encoding, done) {
    this.data.push(chunk);
    console.log(chunk.payload);
    done();
  }
}

class EncryptData {
  constructor(data) {
    this.meta = {
      source: 'ui',
    };

    this.payload = {
      name: data.name,
      email: this.encrypt(data.email),
      password: this.encrypt(data.password),
    };
  }

  encrypt(str) {
    return [...str].reduce((acc, char) => acc + char.charCodeAt().toString(16), '');
  }
}

class Logger extends Transform {
  constructor(db, options = { objectMode: true }) {
    super(options);
    this.db = db;
  }

  _transform(chunk, encoding, done) {
    this.push(chunk);
    this.db.emit('addUser', chunk);
    done();
  }
}

class DB extends EventEmitter {
  constructor() {
    super();
    this.users = [];
    this._handleEvents();
  }

  _handleEvents() {
    this.on('addUser', this._onAddUser);
  }

  _onAddUser(data) {
    const { meta, payload } = data;

    this._saveUserData({
      source: meta.source,
      payload,
    });
  }

  _saveUserData(data) {
    this.users.push({
      ...data,
      created: new Date(),
    });
  }
}

module.exports = {
  Ui,
  Guardian,
  AccountManager,
  Logger,
  DB,
};
