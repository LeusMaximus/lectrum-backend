const { Readable, Writable, Transform } = require('stream');

const decryptionAlgorithms = new Map([
  [
    'hex',
    str => {
      const hexChunks = str.match(/.{1,2}/g);

      return hexChunks.reduce((acc, chunk) => acc + String.fromCharCode(parseInt(chunk, 16)), '');
    },
  ],
  [
    'base64',
    str => Buffer.from(str, 'base64').toString(),
  ],
]);

const decryptString = (str, algorithm) => decryptionAlgorithms.get(algorithm)(str);

const decryptUserData = data => {
  const { algorithm } = data.meta;
  const { name, email, password } = data.payload;

  return {
    name,
    email: decryptString(email, algorithm),
    password: decryptString(password, algorithm),
  };
};

class Ui extends Readable {
  constructor(data, options = { objectMode: true }) {
    super(options);

    this._data = [...data];
  }

  _read() {
    const data = this._data.shift();

    if (data) {
      this._validate(data);
      this.push(data);
    } else {
      this.push(null);
    }
  }

  _validate(data) {
    const { meta, payload } = data;

    if (!meta) {
      this.emit('error', 'meta field is required');
    }

    if (!decryptionAlgorithms.has(meta.algorithm)) {
      this.emit('error', `Wrong decryption algorithm ${meta.algorithm}`);
    }

    if (!payload) {
      this.emit('error', 'payload field is required');
    }

    const requiredPayloadFields = ['name', 'email', 'password'];

    requiredPayloadFields.forEach(field => {
      const value = payload[field];
      const requiredMessage = `User ${field} field is required.`;

      if (!value) {
        this.emit('error', requiredMessage);
      }

      if (typeof value !== 'string') {
        this.emit('error', `The ${field} field is not in the correct format. It should be a string.`);
      }

      if (!value.trim()) {
        this.emit('error', requiredMessage);
      }
    });

    if (requiredPayloadFields.length !== Object.keys(payload).length) {
      this.emit('error', `Data for ${payload.name} user has extra fields`);
    }
  }
}

class Decryptor extends Transform {
  constructor(options = { objectMode: true }) {
    super(options);
  }

  _transform(chunk, encoding, done) {
    this.push(decryptUserData(chunk));
    done();
  }
}

class AccountManager extends Writable {
  constructor(options = { objectMode: true }) {
    super(options);
    this.data = [];
  }

  _write(chunk, encoding, done) {
    this.data.push(chunk);
    console.log(chunk);
    done();
  }
}

module.exports = {
  Ui,
  Decryptor,
  AccountManager,
};
